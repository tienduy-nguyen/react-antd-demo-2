import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { PATH } from 'src/constants/paths';
import { Loading } from 'src/components/Loading';
const Files = lazy(() => import('src/pages/FilesPage'));

export const FilesRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={PATH.FILES}
        component={() => (
          <Suspense fallback={<Loading />}>
            <Files />
          </Suspense>
        )}
      />
    </Switch>
  );
};
