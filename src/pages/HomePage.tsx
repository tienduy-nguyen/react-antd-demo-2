import React from 'react';
import { MainLayout } from 'src/layouts/MainLayout';
import { Home } from 'src/components/Home';

const _HomePage = () => {
  return (
    <MainLayout>
      <Home />
    </MainLayout>
  );
};

const HomePage = React.memo(_HomePage);
export default HomePage;
