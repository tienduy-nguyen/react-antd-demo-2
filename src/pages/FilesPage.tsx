import React from 'react';
import { MainLayout } from 'src/layouts/MainLayout';
import { Files } from 'src/components/Files';

const _FilesPage = () => {
  return (
    <MainLayout>
      <Files />
    </MainLayout>
  );
};

const FilesPage = React.memo(_FilesPage);
export default FilesPage;
