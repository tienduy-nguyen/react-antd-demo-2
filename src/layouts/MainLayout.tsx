import React, { ReactNode } from 'react';
import { AppHeader } from 'src/components/Header';
import { AppFooter } from 'src/components/Footer';
import { Layout } from 'antd';
const { Header, Content, Footer } = Layout;

interface Props {
  children: ReactNode;
}

export const MainLayout = (props: Props) => {
  const { children } = props;
  const contentStyle = {
    marginTop: '100px',
    marginBottom: '50px',
  };
  return (
    <Layout className="mainLayout">
      <Header>
        <AppHeader />
      </Header>
      <Content>
        <div style={contentStyle}>{children}</div>
      </Content>
      <Footer>
        <AppFooter />
      </Footer>
    </Layout>
  );
};
