# react-antd-demo-2

Simple project demo with React, TypeScript, Ant Design

Demo at [react-ant-demo-2.netlify.app](https://react-ant-demo-2.netlify.app/)

- [react-antd-demo-2](#react-antd-demo-2)
  - [Getting Started](#getting-started)
    - [Init project](#init-project)
    - [Create Json server](#create-json-server)
  - [Create Layout, Components & Routes](#create-layout-components--routes)
## Getting Started

### Init project

- Using [create-react-app](https://create-react-app.dev/) to create project
  ```bash
  $ yarn create react-app react-antd-demo --template typescript
  # or
  $ npm create-react-app react-antd-demo --template typescript
  ```
- Install important dependencies

  If you want to create  a complex app with Router and Management State, you need to install more dependencies after using create-react-app

  ```bash
  $ yarn add redux react-redux react-router react-router-dom redux-thunk axios
  $ yarn add @types/redux @types/react-redux @types/react-router @types/react-router-dom @types/redux-thunk
  ```
- Install **Ant Design**
  ```bash
  $ yarn add antd @ant-design/icons
  ```
- Install package `node-sass`
  It allows import and use **scss** or **sass** in React project
  ```bash
  $ yarn add node-sass@4.14.1
  ```
  Why use node-sass@4.14.1? Because, in the moment that I work in this project, there are some bugs of the latest version of **node-sass** with React.

- If you want to use ESLint & Prettier in your project, you can check my blog [how to config ESlint & Prettier in React Project](https://blog.adev42.com/config-esling-prettier-react-app)

### Create Json server
To simplify for the demo, I will use the [json-server](https://github.com/typicode/json-server) to provide the data for backend.

- Install json-server
  ```bash
  $ yarn add global json-server
  # or
  $ npm i -g json-server
  ```
- Create database
  In the root project, create  `server` folder, and `server/db.json` file
  ```bash
  $ mkdir server
  $ touch server/db.json
  ```
  In `db.json` add the following code:

  ```json
  {
    "users":[]
  }
  ```
- Create `json-server.json` file to add some config for json server.

  Here, we add only the port of your server
  ```json
  {
    "port": 500
  }
  ```
- Run server
  Using your new terminal to run server in the root project
  ```bash
  $ json-server --watch server/db.json
  ```
  If you want to run server & client same time on your terminal, please check awesome package [concurrently](https://github.com/kimmobrunfeldt/concurrently)

## Create Layout, Components & Routes
Check in the files projects