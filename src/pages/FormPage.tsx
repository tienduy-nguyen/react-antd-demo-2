import React from 'react';
import { MainLayout } from 'src/layouts/MainLayout';
import { AppForm } from 'src/components/Form';

const _FormPage = () => {
  return (
    <MainLayout>
      <AppForm />
    </MainLayout>
  );
};

const FormPage = React.memo(_FormPage);
export default FormPage;
