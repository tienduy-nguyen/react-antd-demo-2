export const PATH = {
  HOME: '/',
  LIST: '/list',
  FORM: '/form',
  FILES: '/files',
  VIDEOS: '/videos',
};
