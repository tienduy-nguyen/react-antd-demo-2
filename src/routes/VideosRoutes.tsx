import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { PATH } from 'src/constants/paths';
import { Loading } from 'src/components/Loading';
const Videos = lazy(() => import('src/pages/VideosPage'));

export const VideosRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={PATH.VIDEOS}
        component={() => (
          <Suspense fallback={<Loading />}>
            <Videos />
          </Suspense>
        )}
      />
    </Switch>
  );
};
