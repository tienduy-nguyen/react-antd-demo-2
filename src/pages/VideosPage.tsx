import React from 'react';
import { MainLayout } from 'src/layouts/MainLayout';
import { Videos } from 'src/components/Videos';

const _VideosPage = () => {
  return (
    <MainLayout>
      <Videos />
    </MainLayout>
  );
};

const VideosPage = React.memo(_VideosPage);
export default VideosPage;
