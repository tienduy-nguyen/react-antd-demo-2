import React, { useEffect, useState } from 'react';
import { MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons';
import { SideNav } from 'src/components/SideNav';
import { Layout } from 'antd';
const { Sider } = Layout;

export const AppHeader = () => {
  const [collapsed, setCollapse] = useState(false);
  useEffect(() => {
    window.innerWidth <= 760 ? setCollapse(true) : setCollapse(false);
  }, []);
  const handleToggle = (e: React.MouseEvent) => {
    e.preventDefault();
    setCollapse(!collapsed);
  };
  return (
    <div>
      <div
        className="siteLayoutBackground"
        style={{ padding: 0, background: '#001529' }}
      >
        {React.createElement(
          collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
          {
            className: 'trigger',
            onClick: handleToggle,
            style: { color: '#fff' },
          },
        )}
      </div>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <SideNav />
      </Sider>
    </div>
  );
};
