import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { PATH } from 'src/constants/paths';
import { Loading } from 'src/components/Loading';
const Form = lazy(() => import('src/pages/FormPage'));

export const FormRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={PATH.FORM}
        component={() => (
          <Suspense fallback={<Loading />}>
            <Form />
          </Suspense>
        )}
      />
    </Switch>
  );
};
