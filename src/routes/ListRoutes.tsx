import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { PATH } from 'src/constants/paths';
import { Loading } from 'src/components/Loading';
const List = lazy(() => import('src/pages/ListPage'));

export const ListRoutes = () => {
  return (
    <Switch>
      <Route
        exact
        path={PATH.LIST}
        component={() => (
          <Suspense fallback={<Loading />}>
            <List />
          </Suspense>
        )}
      />
    </Switch>
  );
};
