import React from 'react';
import { MainLayout } from 'src/layouts/MainLayout';
import { List } from 'src/components/List';

const _ListPage = () => {
  return (
    <MainLayout>
      <List />
    </MainLayout>
  );
};

const ListPage = React.memo(_ListPage);
export default ListPage;
