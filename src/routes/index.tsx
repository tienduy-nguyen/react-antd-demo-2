import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { HomeRoutes } from './HomeRoutes';
import { ListRoutes } from './ListRoutes';
import { FormRoutes } from './FormRoutes';
import { VideosRoutes } from './VideosRoutes';
import { FilesRoutes } from './FilesRoutes';
export const Routes = () => {
  return (
    <BrowserRouter>
      <HomeRoutes />
      <ListRoutes />
      <FormRoutes />
      <VideosRoutes />
      <FilesRoutes />
    </BrowserRouter>
  );
};
